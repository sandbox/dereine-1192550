<?php

function bookmark_form($form, $form_state, $bookmark = NULL) {
  if (!isset($bookmark)) {
    $bookmark = new stdClass();
    $bookmark->uid = $GLOBALS['user']->uid;
    $bookmark->language = LANGUAGE_NONE;
    $bookmark->url = '';
    $bookmark->title = '';
    $bookmark->status = BOOKMARK_PUBLIC;
  }

  if (!isset($form_state['bookmark'])) {
    bookmark_object_prepare($bookmark);
    $form_state['bookmark'] = $bookmark;
  }
  else {
    $bookmark = $form_state['bookmark'];
  }

  $form['#attributes']['class'][] = 'bookmark-form';

  // Basic bookmark information.
  // These elements are just values so they are not even sent to the client.
  foreach (array('bid', 'uid', 'created', 'language') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($bookmark->$key) ? $bookmark->$key : NULL,
    );
  }

  // Changed must be sent to the client, for later overwrite error checking.
  $form['changed'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($bookmark->changed) ? $bookmark->changed : NULL,
  );

  $form['url'] = array(
    '#type' => module_exists('elements') ? 'urlfield' : 'textfield',
    '#title' => t('URL'),
    '#default_value' => $bookmark->url,
    '#maxlength' => NULL,
//     '#element_validate' => array('bookmark_url_element_validate'),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $bookmark->title,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Public bookmark'),
    '#default_value' => $bookmark->status,
    '#return_value' => BOOKMARK_PUBLIC,
    '#weight' => 50,
  );

  // Add the buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('bookmark_form_submit'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : 'bookmarks',
    '#weight' => 5,
  );

  $form['#validate'][] = 'bookmark_form_validate';
  $form['#submit'] = array();

  field_attach_form('bookmark', $bookmark, $form, $form_state, $bookmark->language);
  return $form;
}

function bookmark_delete_form($form, &$form_state, $bookmark) {
  $form['#bookmark'] = $bookmark;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['bid'] = array(
    '#type' => 'value',
    '#value' => $bookmark->bid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the bookmark for %url?', array('%url' => $bookmark->url)),
    'bookmark/' . $bookmark->bid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function bookmark_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $bookmark = bookmark_load($form_state['values']['bid']);
    bookmark_delete($form_state['values']['bid']);
    watchdog('bookmark', 'Bookmark: deleted %title.', array('%title' => $bookmark->title));
    drupal_set_message(t('Bookmark %title has been deleted.', array('%title' => $bookmark->title)));
  }

  $form_state['redirect'] = 'bookmark';
}
